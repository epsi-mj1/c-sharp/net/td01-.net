﻿using System;

namespace Exam.Impl
{
    public class FizzBuzzService
    {
        public string Play(int upperBound)
        {
            string result = "";
            for (int i = 1; i <= upperBound; i++)
            {
                if (i % 3 == 0 && i % 5 == 0)
                {
                    result += "FizzBuzz";
                }
                else if (i % 3 == 0)
                {
                    result += "Fizz";
                }
                else if (i % 5 == 0)
                {
                    result += "Buzz";
                }
                else
                {
                    result += i.ToString();
                }
            }
            return result;
        }
    }
}