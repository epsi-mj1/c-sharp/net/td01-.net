﻿using System;

namespace Exam.Impl
{
    public class Armstrong
    {
        public bool IsArmstrong(int input)
        {
            int rest;
            int sum = 0;
            int temp = input;

            while (input > 0)
            {

                rest = input % 10;

                sum = sum + (rest * rest * rest);
                input = input / 10;

            }

            if (temp == sum)
            {
                bool isBool = true;
                return isBool;
            }
            else
            {
                bool isBool = false;
                return isBool;
            }

        }
    }
}