﻿using System;
using System.ComponentModel.DataAnnotations;


namespace MVCDisk.Models
{
    public class Disk
    {
        public int Id { get; set; }
        public string Title { get; set; }

        [DataType(DataType.Date)]
        public DateTime ReleaseDate { get; set; }
       
        public string Interprete { get; set; }

    }
}
