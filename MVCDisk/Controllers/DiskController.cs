﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MVCDisk.Models;

namespace MVCDisk.Controllers
{
    public class DiskController : Controller
    {
        private static List<Disk> Disks = new List<Disk>
        {
            new Disk
            {
                Id = 1,
                Title = "Souldier",
                Interprete = "Jain",
                ReleaseDate = new DateTime(2018, 08, 24)

            },
            new Disk
            {
                Id = 2,
                Title = "Origins",
                Interprete = "Imagine Dragons",
                ReleaseDate = new DateTime(2018, 11, 09)

            }, 
            new Disk
            {
                Id = 3,
                Title = "En amont",
                Interprete = "Alain Bashung",
                ReleaseDate = new DateTime(2018, 11, 23)

            },
             new Disk
            {
                Id = 4,
                Title = "La vie de reve",
                Interprete = "Bigflo et Oli",
                ReleaseDate = new DateTime(2018, 11, 23)

            },
        };
        // GET: Disk
        public ActionResult Index(string searchString)
        {
            var disks = Disks;
            if (!string.IsNullOrEmpty(searchString))
            {
                disks = disks.Where(s => s.Title.Contains(searchString)).ToList();
            }
            return View(disks);
        }

        // GET: Disk/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Disk/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Disk/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind("Title,ReleaseDate,Interprete")] Disk disk)
        {
            try
            {
                disk.Id = Disks.Max(m => m.Id) + 1;
                Disks.Add(disk);


                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Disk/Edit/5
        public ActionResult Edit(int id)
        {
            return View(Disks.Single(disk => disk.Id == id));
        }

        // POST: Disk/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, [Bind("ID,Title,ReleaseDate,Interprete")] Disk disk)
        {
            try
            {
                var diskToUpdate = Disks.Single(d => d.Id == id);
                diskToUpdate.Interprete = disk.Interprete;
                diskToUpdate.ReleaseDate = disk.ReleaseDate;
                diskToUpdate.Title = disk.Title;


                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }

        // GET: Disk/Delete/5
        public ActionResult Delete(int id)
        {

            return View(Disks.Single(disk => disk.Id == id));

        }

        // POST: Disk/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id,[Bind("Id")] Disk disk)
        {
            try
            {
                Disks.Remove(Disks.Single(d => d.Id == id));


                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View();
            }
        }
    }
}